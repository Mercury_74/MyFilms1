﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MyFilms1.Models
{
    public class MyFilms1Context : DbContext
    {
        public MyFilms1Context (DbContextOptions<MyFilms1Context> options)
            : base(options)
        {
        }

        public DbSet<MyFilms1.Models.Film> Film { get; set; }
    }
}
